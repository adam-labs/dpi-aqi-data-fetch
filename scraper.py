from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import json


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (
        resp.status_code == 200
        and content_type is not None
        and content_type.find('html') > -1
    )


def log_error(e):
    print(e)


def parse_row(row):
    """
    Create dictionary of AQI data from html table row
    """
    row_aqi = {}
    headers = [
        "site",
        "ozone_1h",
        "ozone_4h",
        "nitrogen_dioxide",
        "visibility",
        "carbon_monoxide",
        "sulfur_dioxide",
        "particles_pm10",
        "particles_pm2_5",
        "site_aqi",
    ]
    readings = [x for x in row.find_all('td', class_=True)]
    if "region" in readings[0]["class"]:
        readings.remove(readings[0])
    for index, header in enumerate(headers):
        row_aqi[header] = readings[index].string
    return row_aqi


def get_aqi_table(html):
    aqi_readings = []
    aqi_table = html.find('table', class_="aqi")
    aqi_sites = aqi_table.find_all('td', class_="site")
    for aqi_site in aqi_sites:
        aqi_row = aqi_site.find_parent('tr')
        aqi_reading = parse_row(aqi_row)
        aqi_readings.append(aqi_reading)
    return aqi_readings


def get_aqi_date(html):
    date_td = html.find('td', class_="date")
    date_href = date_td.find('a')["href"]
    for query_param in date_href.split("&"):
        if "date=" in query_param:
            return query_param.replace("date=", "")


def get_current_aqi():
    url = 'https://airquality.environment.nsw.gov.au/aquisnetnswphp/getPage.php?reportid=1'
    response = simple_get(url)
    if response is not None:
        html = BeautifulSoup(response, 'html.parser')
        current_aqi = {
            "aqi": get_aqi_table(html),
            "timestamp": get_aqi_date(html)
        }
        return json.dumps(current_aqi)


if __name__ == "__main__":
    print(get_current_aqi())
